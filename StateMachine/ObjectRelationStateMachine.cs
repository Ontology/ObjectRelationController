﻿using ObjectRelationController.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.StateMachines;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelationController.StateMachine
{
    public delegate void SendClassAttributeNode(clsOntologyItem oItemSelected, ClassAttributeNode classAttributeNode);
    public delegate void SendObjRelLeftRight(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode);
    public delegate void SendObjRelRightLeft(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode);
    public delegate void SendObjRelLeftRightInformal(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode);
    public delegate void SendObjRelRightLeftInformal(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode);

    public class ObjectRelationStateMachine : ControllerStateMachine
    {
        private ServiceAgent_Elastic serviceAgentElastic;

        public event SendClassAttributeNode sendClassAttributeNode;
        public event SendObjRelLeftRight sendObjRelLeftRight;
        public event SendObjRelRightLeft sendObjRelRightLeft;
        public event SendObjRelLeftRightInformal sendObjRelLeftRightInformal;
        public event SendObjRelRightLeftInformal sendObjRelRightLeftInformal;


        private ObjectRelNode selectedObjectRelNodeLeftright;
        public ObjectRelNode SelectedObjectRelNodeLeftright
        {
            get { return selectedObjectRelNodeLeftright; }
            set
            {
                selectedObjectRelNodeLeftright = value;
                RaisePropertyChanged(nameof(SelectedObjectRelNodeLeftright));
            }
        }

        private ObjectRelNode selectedObjectRelNodeLeftRightInformal;
        public ObjectRelNode SelectedObjectRelNodeLeftRightInformal
        {
            get { return selectedObjectRelNodeLeftRightInformal; }
            set
            {
                selectedObjectRelNodeLeftRightInformal = value;
                RaisePropertyChanged(nameof(SelectedObjectRelNodeLeftRightInformal));
            }
        }

        private ObjectRelNode selectedObjectRelNodeRightLeft;
        public ObjectRelNode SelectedObjectRelNodeRightLeft
        {
            get { return selectedObjectRelNodeRightLeft; }
            set
            {
                selectedObjectRelNodeRightLeft = value;
                RaisePropertyChanged(nameof(SelectedObjectRelNodeRightLeft));
            }
        }

        private ObjectRelNode selectedObjectRelNodeRightLeftInformal;
        public ObjectRelNode SelectedObjectRelNodeRightLeftInformal
        {
            get { return selectedObjectRelNodeRightLeftInformal; }
            set
            {
                selectedObjectRelNodeRightLeftInformal = value;
                RaisePropertyChanged(nameof(SelectedObjectRelNodeRightLeftInformal));
            }
        }


        private ClassAttributeNode selectedClassAttribute;
        public ClassAttributeNode SelectedClassAttribute
        {
            get { return selectedClassAttribute; }
            set
            {
                selectedClassAttribute = value;
                RaisePropertyChanged(nameof(SelectedClassAttribute));
            }
        }

        

        public void SetRelationNode(string idRelationNode)
        {
            if (!LoginSuccessful) return;
            if (OItemSelected == null) return;

            var selectedClassAttribute = serviceAgentElastic.ClassAttributeNodes.FirstOrDefault(clsAttNode => clsAttNode.IdNode == idRelationNode);
            if (selectedClassAttribute != null)
            {
                SelectedClassAttribute = selectedClassAttribute;
                SelectedObjectRelNodeLeftright = null;
                SelectedObjectRelNodeRightLeft = null;
                SelectedObjectRelNodeLeftRightInformal = null;
                SelectedObjectRelNodeRightLeftInformal = null;
                sendClassAttributeNode?.Invoke(OItemSelected, SelectedClassAttribute);
                
                return;
            }

            var selectedObjRelLeftRight = serviceAgentElastic.LeftRightNodes.FirstOrDefault(leftRightNode => leftRightNode.IdNode == idRelationNode);
            if (selectedObjRelLeftRight != null)
            {
                SelectedObjectRelNodeLeftright = selectedObjRelLeftRight;
                SelectedClassAttribute = null;
                SelectedObjectRelNodeRightLeft = null;
                SelectedObjectRelNodeLeftRightInformal = null;
                SelectedObjectRelNodeRightLeftInformal = null;
                sendObjRelLeftRight?.Invoke(OItemSelected, SelectedObjectRelNodeLeftright);
                return;
            }

            var selectedObjRelRightLeft = serviceAgentElastic.RightLeftNodes.FirstOrDefault(leftRightNode => leftRightNode.IdNode == idRelationNode);

            if (selectedObjRelRightLeft != null)
            {
                SelectedObjectRelNodeRightLeft = selectedObjRelRightLeft;
                SelectedClassAttribute = null;
                SelectedObjectRelNodeLeftright = null;
                SelectedObjectRelNodeLeftRightInformal = null;
                SelectedObjectRelNodeRightLeftInformal = null;
                sendObjRelRightLeft?.Invoke(OItemSelected, SelectedObjectRelNodeRightLeft);
                return;
            }

            var selectedObjRelLeftRightInformal = serviceAgentElastic.LeftRightInformalNodes.FirstOrDefault(leftRightNode => leftRightNode.IdNode == idRelationNode);

            if (selectedObjRelLeftRightInformal != null)
            {
                SelectedObjectRelNodeLeftRightInformal = selectedObjRelLeftRightInformal;
                SelectedClassAttribute = null;
                SelectedObjectRelNodeLeftright = null;
                SelectedObjectRelNodeRightLeft = null;
                SelectedObjectRelNodeRightLeftInformal = null;
                sendObjRelLeftRightInformal(OItemSelected, SelectedObjectRelNodeLeftRightInformal);
                return;
            }

            var selectedObjRelRightLeftInformal = serviceAgentElastic.RightLeftInformalNodes.FirstOrDefault(leftRightNode => leftRightNode.IdNode == idRelationNode);

            if (selectedObjRelRightLeftInformal != null)
            {
                SelectedObjectRelNodeRightLeftInformal = selectedObjRelRightLeftInformal;
                SelectedClassAttribute = null;
                SelectedObjectRelNodeLeftright = null;
                SelectedObjectRelNodeRightLeft = null;
                SelectedObjectRelNodeLeftRightInformal = null;
                sendObjRelRightLeftInformal(OItemSelected, SelectedObjectRelNodeRightLeftInformal);
                return;
            }
        }
        public ObjectRelationStateMachine(StateMachineType stateMachineType, ServiceAgent_Elastic serviceAgentElastic) : base(stateMachineType)
        {
            this.serviceAgentElastic = serviceAgentElastic;
        }
    }
}
