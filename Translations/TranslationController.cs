﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelationController.Translations
{
    public class TranslationController
    {

        
        public string Title_FileStreamError
        {
            get
            {
                return GetValue("244d7e5cd2e64401bd99a91ba98fda34", "Datei-Fehler!");
            }

        }

        public string Text_FileStreamError
        {
            get
            {
                return GetValue("5682b2ddf0be4d1babeac59245d25a85", "Datei für Datenaustausch konnte  nicht agespeichert werden!");
            }

        }

        public string Title_WebConfigError
        {
            get
            {
                return GetValue("78634024801646ceba093bb8c62f7f0a", "Webserver-Fehler");
            }

        }

        public string Text_WebConfigError
        {
            get
            {
                return GetValue("7d1199b177ba435b8c2bcb917cec913b", "Der Webserver hat einen Fehler verursacht!");
            }

        }


        public string Text_Attributes
        {
            get
            {
                return GetValue("a", "Attribute");
            }
        }

        public string Text_LeftRight
        {
            get
            {
                return GetValue("b", "Aktive Beziehungen");
            }
        }

        public string Text_LeftRightInformal
        {
            get
            {
                return GetValue("c", "Aktive informale Beziehungen");
            }
        }

        public string Text_RightLeft
        {
            get
            {
                return GetValue("c", "Passive Beziehungen");
            }
        }

        public string Text_Other
        {
            get
            {
                return GetValue("d", "Passsiv informale Beziehungen");
            }
        }

        public string MenuEntry_AddAttribute
        {
            get
            {
                return GetValue("MenuEntry.AddAttribute", "Attribute hinzufügen");
            }
        }

        public string MenuEntry_Relation
        {
            get
            {
                return GetValue("MenuEntry.Relation", "Beziehung");
            }
        }

        public string MenuEntry_NextOrderId
        {
            get
            {
                return GetValue("MenuEntry.NextOrderId", "Nächste Order-Id");
            }
        }

        public string MenuEntry_Listen
        {
            get
            {
                return GetValue("MenuEntry.Listen", "Listen");
            }
        }

        public string GetTranslatedByPropertyName(string propertyName)
        {
            var property = this.GetType().GetProperties().Cast<PropertyInfo>().FirstOrDefault(propItem => propItem.Name == propertyName);

            if (property != null)
            {
                var value = property.GetValue(this);
                return value != null ? value.ToString() : "xxx_Error_xxx";
            }
            else
            {
                return "xxx_Error_xxx";
            }
        }


        private string GetValue(string IdReference, string defaultValue)
        {
            return defaultValue;
        }

    }
}
