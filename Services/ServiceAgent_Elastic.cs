﻿using ObjectRelationController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ReferenceTreeController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ObjectRelationController.Services
{
    
    public class ServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReader_ClassAttributes;
        private OntologyModDBConnector dbReader_ObjectAttributes;
        private OntologyModDBConnector dbReader_ClassRelationsLeftRight;
        private OntologyModDBConnector dbReader_ObjectRelationsLeftRight;
        private OntologyModDBConnector dbReader_ClassRelationsRightLeft;
        private OntologyModDBConnector dbReader_ObjectRelationsRightLeft;
        private OntologyModDBConnector dbReader_ClassRelationsLeftRightInformal;
        private OntologyModDBConnector dbReader_ObjectRelationsLeftRightInformal;
        private OntologyModDBConnector dbReader_ClassRelationsRightLeftInformal;
        private OntologyModDBConnector dbReader_ObjectRelationsRightLeftInformal;
        private OntologyModDBConnector dbReader_OItem;

        private Thread threadAttributes;
        private Thread threadLeftRight;
        private Thread threadRightLeft;
        private Thread threadLeftRightInformal;
        private Thread threadRightLeftInformal;


        private object serviceAgentLocker = new object();

        private List<ClassAttributeNode> classAttributeNodes;
        public List<ClassAttributeNode> ClassAttributeNodes
        {
            get { return classAttributeNodes; }
            set
            {
                classAttributeNodes = value;
            }
        }

        private List<ObjectRelNode> leftRightNodes;
        public List<ObjectRelNode> LeftRightNodes
        {
            get { return leftRightNodes; }
            set
            {
                leftRightNodes = value;
            }

        }

        private List<ObjectRelNode> rightLeftNodes;
        public List<ObjectRelNode> RightLeftNodes
        {
            get { return rightLeftNodes; }
            set
            {
                rightLeftNodes = value;
            }

        }

        private List<ObjectRelNode> leftRightInformalNodes;
        public List<ObjectRelNode> LeftRightInformalNodes
        {
            get { return leftRightInformalNodes; }
            set
            {
                leftRightInformalNodes = value;
            }

        }

        private List<ObjectRelNode> rightLeftInformalNodes;
        public List<ObjectRelNode> RightLeftInformalNodes
        {
            get { return rightLeftInformalNodes; }
            set
            {
                rightLeftInformalNodes = value;
            }

        }

        private clsOntologyItem resultDataAttributes;
        public clsOntologyItem ResultDataAttributes
        {
            get { return resultDataAttributes; }
            set
            {
                resultDataAttributes = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultDataAttributes);
            }
        }

        private clsOntologyItem resultDataLeftRight;
        public clsOntologyItem ResultDataLeftRight
        {
            get { return resultDataLeftRight; }
            set
            {
                resultDataLeftRight = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultDataLeftRight);
            }
        }

        private clsOntologyItem resultDataRightLeft;
        public clsOntologyItem ResultDataRightLeft
        {
            get { return resultDataRightLeft; }
            set
            {
                resultDataRightLeft = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultDataRightLeft);
            }
        }

        private clsOntologyItem resultDataLeftRightInformal;
        public clsOntologyItem ResultDataLeftRightInformal
        {
            get { return resultDataLeftRightInformal; }
            set
            {
                resultDataLeftRightInformal = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultDataLeftRightInformal);
            }
        }

        private clsOntologyItem resultDataRightLeftInformal;
        public clsOntologyItem ResultDataRightLeftInformal
        {
            get { return resultDataRightLeftInformal; }
            set
            {
                resultDataRightLeftInformal = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultDataRightLeftInformal);
            }
        }

        public ServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        public clsOntologyItem LoadAttributes(clsOntologyItem objectItem)
        {
            try
            {
                if (threadAttributes != null)
                {
                    threadAttributes.Abort();
                }
            }
            catch(Exception ex)
            {

            }

            threadAttributes = new Thread(LoadAttributesAsync);
            threadAttributes.Start(objectItem);
            return localConfig.Globals.LState_Success.Clone();
        }

        public clsOntologyItem LoadLeftRight(clsOntologyItem objectItem)
        {
            
            try
            {
                if (threadLeftRight != null)
                {
                    threadLeftRight.Abort();
                }
            }
            catch (Exception ex)
            {

            }

            threadLeftRight = new Thread(LoadLeftRightAsync);
            threadLeftRight.Start(objectItem);
            return localConfig.Globals.LState_Success.Clone();
        }

        private void LoadLeftRightAsync(object item)
        {
            var oItem = (clsOntologyItem)item;
            var result = GetData_001_ClassRelationsForward(oItem);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultDataLeftRight = result;
                return;
            }

            result = GetData_002_ObjectRelationsForward(oItem);

            ResultDataLeftRight = result;


        }

        private void LoadAttributesAsync(object item)
        {
            var oItem = (clsOntologyItem)item;
            var result = GetData_001_ClassAttributes(oItem);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultDataAttributes = result;
                return;
            }

            result = GetData_002_ObjectAttributes(oItem);

            ResultDataAttributes = result;


        }

        public clsOntologyItem LoadRightLeft(clsOntologyItem objectItem)
        {
            try
            {
                if (threadRightLeft != null)
                {
                    threadRightLeft.Abort();
                }
            }
            catch (Exception ex)
            {

            }

            threadRightLeft = new Thread(LoadRightLeftAsync);
            threadRightLeft.Start(objectItem);
            return localConfig.Globals.LState_Success.Clone();
        }

        private void LoadRightLeftAsync(object item)
        {
            var oItem = (clsOntologyItem)item;
            var result = GetData_001_ClassRelationsBackward(oItem);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultDataRightLeft = result;
                return;
            }

            result = GetData_002_ObjectRelationsBackward(oItem);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultDataRightLeft = result;
                return;
            }

            result = GetData_003_ObjectRelationsBackwardInformal(oItem);


            ResultDataRightLeft = result;


        }

        public clsOntologyItem LoadLeftRightInformal(clsOntologyItem objectItem)
        {
            
            try
            {
                if (threadLeftRightInformal != null)
                {
                    threadLeftRightInformal.Abort();
                }
            }
            catch (Exception ex)
            {

            }

            threadLeftRightInformal = new Thread(LoadLeftRightInformalAsync);
            threadLeftRightInformal.Start(objectItem);
            return localConfig.Globals.LState_Success.Clone();
        }

        private void LoadLeftRightInformalAsync(object leftRightInformal)
        {
            var oItem = (clsOntologyItem)leftRightInformal;
            var result = GetData_001_ClassRelationsForwardInformal(oItem);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultDataLeftRightInformal = result;
                return;
            }

            result = GetData_002_ObjectRelationsForwardInformal(oItem);

            ResultDataLeftRightInformal = result;


        }


        private clsOntologyItem GetData_001_ClassAttributes(clsOntologyItem oItem)
        {
            var searchClassAttributes = new List<clsClassAtt>
            {
                new clsClassAtt
                {
                    ID_Class = oItem.Type == localConfig.Globals.Type_Object ? oItem.GUID_Parent : oItem.GUID
                }
            };

            var result = dbReader_ClassAttributes.GetDataClassAtts(searchClassAttributes);

            return result;
        }

        private clsOntologyItem GetData_002_ObjectAttributes(clsOntologyItem oItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            int idNode = 0;

            lock (serviceAgentLocker)
            {
                ClassAttributeNodes = dbReader_ClassAttributes.ClassAtts.Select(clsAtt => new ClassAttributeNode
                {
                    IdNode = "att" + (idNode++).ToString(),
                    IdClass = clsAtt.ID_Class,
                    IdAttributeType = clsAtt.ID_AttributeType,
                    NameClass = clsAtt.Name_Class,
                    NameAttributeType = clsAtt.Name_AttributeType,
                    Min = clsAtt.Min.Value,
                    Max = clsAtt.Max.Value
                }).ToList();


            }

            if (oItem.Type == localConfig.Globals.Type_Object)
            {
                

                ClassAttributeNodes.ForEach(clsAtt =>
                {
                    var searchObjectAtt = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_AttributeType = clsAtt.IdAttributeType,
                            ID_Class = clsAtt.IdClass,
                            ID_Object = oItem.GUID
                        }
                    };

                    result = dbReader_ObjectAttributes.GetDataObjectAtt(searchObjectAtt, doCount: true);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        clsAtt.Count = result.Count.Value;
                    }
                });
            }
            
            return result;
        }

        private clsOntologyItem GetData_001_ClassRelationsForward(clsOntologyItem oItem)
        {
            var searchClassRelationsForward = new List<clsClassRel>
            {
                new clsClassRel
                {
                    ID_Class_Left = oItem.Type == localConfig.Globals.Type_Object ? oItem.GUID_Parent : oItem.GUID
                }
            };

            var result = dbReader_ClassRelationsLeftRight.GetDataClassRel(searchClassRelationsForward);

            return result;
        }

        private clsOntologyItem GetData_002_ObjectRelationsForward(clsOntologyItem oItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            int idNode = 0;
            lock (serviceAgentLocker)
            {
                LeftRightNodes = dbReader_ClassRelationsLeftRight.ClassRels.Where(clsRel => !string.IsNullOrEmpty(clsRel.ID_Class_Right)).ToList().Select(clsRel => new ObjectRelNode(true, false)
                {
                    IdNode = "relforw" + (idNode++).ToString(),
                    IdLeft = clsRel.ID_Class_Left,
                    NameLeft = clsRel.Name_Class_Left,
                    IdRelationType = clsRel.ID_RelationType,
                    NameRelationType = clsRel.Name_RelationType,
                    IdRight = clsRel.ID_Class_Right,
                    NameRight = clsRel.Name_Class_Right,
                    Min = clsRel.Min_Forw.Value,
                    MaxBackw = clsRel.Max_Backw.Value,
                    MaxForw = clsRel.Max_Forw.Value,
                    NodeType = NodeType.NodeForwardFormal
                }).ToList();


            }

            if (oItem.Type == localConfig.Globals.Type_Object)
            {
                LeftRightNodes.ForEach(clsRel =>
                {
                    var searchObjectRel = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = oItem.GUID,
                            ID_Parent_Other = clsRel.IdRight,
                            ID_RelationType = clsRel.IdRelationType,
                        }
                    };

                    result = dbReader_ObjectRelationsLeftRight.GetDataObjectRel(searchObjectRel, doCount: true);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        clsRel.Count = result.Count.Value;
                    }
                });
            }
            

            return result;
        }

        private clsOntologyItem GetData_001_ClassRelationsBackward(clsOntologyItem oItem)
        {
            var searchClassRelationsBackward = new List<clsClassRel>
            {
                new clsClassRel
                {
                    ID_Class_Right = oItem.Type == localConfig.Globals.Type_Object ? oItem.GUID_Parent : oItem.GUID
                }
            };

            var result = dbReader_ClassRelationsRightLeft.GetDataClassRel(searchClassRelationsBackward);

            return result;
        }

        private clsOntologyItem GetData_002_ObjectRelationsBackward(clsOntologyItem oItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            int idNode = 0;
            lock (serviceAgentLocker)
            {
                RightLeftNodes = dbReader_ClassRelationsRightLeft.ClassRels.Select(clsRel => new ObjectRelNode(false, false)
                {
                    IdNode = "relbackw" + (idNode++).ToString(),
                    IdLeft = clsRel.ID_Class_Left,
                    NameLeft = clsRel.Name_Class_Left,
                    IdRelationType = clsRel.ID_RelationType,
                    NameRelationType = clsRel.Name_RelationType,
                    IdRight = clsRel.ID_Class_Right,
                    NameRight = clsRel.Name_Class_Right,
                    Min = clsRel.Min_Forw.Value,
                    MaxBackw = clsRel.Max_Backw.Value,
                    MaxForw = clsRel.Max_Forw.Value,
                    NodeType = NodeType.NodeBackwardFormal
                }).ToList();


            }

            if (oItem.Type == localConfig.Globals.Type_Object)
            {
                RightLeftNodes.ForEach(clsRel =>
                {
                    var searchObjectRel = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = oItem.GUID,
                            ID_Parent_Object = clsRel.IdLeft,
                            ID_RelationType = clsRel.IdRelationType,
                        }
                    };

                    result = dbReader_ObjectRelationsRightLeft.GetDataObjectRel(searchObjectRel, doCount: true);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        clsRel.Count = result.Count.Value;
                    }
                });
            }
            

            return result;
        }


        private clsOntologyItem GetData_001_ClassRelationsForwardInformal(clsOntologyItem oItem)
        {
            
            var searchClassRelationsForward = new List<clsClassRel>
            {
                new clsClassRel
                {
                    ID_Class_Left = oItem.Type == localConfig.Globals.Type_Object ? oItem.GUID_Parent : oItem.GUID
                }
            };

            var result = dbReader_ClassRelationsLeftRightInformal.GetDataClassRel(searchClassRelationsForward,doOr:true);

            return result;
        }

        private clsOntologyItem GetData_002_ObjectRelationsForwardInformal(clsOntologyItem oItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            
            if (oItem.Type == localConfig.Globals.Type_Object)
            {
                int idNode = 0;

                lock (serviceAgentLocker)
                {
                    LeftRightInformalNodes = dbReader_ClassRelationsLeftRightInformal.ClassRels.Where(clsRel => string.IsNullOrEmpty(clsRel.ID_Class_Right)).ToList().Select(clsRel => new ObjectRelNode(true, true)
                    {
                        IdNode = "relforwinf" + (idNode++).ToString(),
                        IdLeft = clsRel.ID_Class_Left,
                        NameLeft = clsRel.Name_Class_Left,
                        IdRelationType = clsRel.ID_RelationType,
                        NameRelationType = clsRel.Name_RelationType,
                        IdRight = oItem.GUID,
                        NameRight = oItem.Name,
                        Min = clsRel.Min_Forw.Value,
                        MaxBackw = clsRel.Max_Backw.Value,
                        MaxForw = clsRel.Max_Forw.Value,
                        NodeType = NodeType.NodeForwardInformal
                    }).ToList();


                }

                LeftRightInformalNodes.ForEach(clsRel =>
                {
                    var searchObjectRel = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = oItem.GUID,
                            ID_RelationType = clsRel.IdRelationType,
                            Ontology = clsRel.Ontology
                        }
                    };

                    result = dbReader_ObjectRelationsLeftRightInformal.GetDataObjectRel(searchObjectRel, doCount: true);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        clsRel.Count = result.Count.Value;
                    }
                });
            }
            

            return result;
        }



        
        private clsOntologyItem GetData_003_ObjectRelationsBackwardInformal(object oRightLeftInformal)
        {
            var oItem = (clsOntologyItem)oRightLeftInformal;
            var result = localConfig.Globals.LState_Success.Clone();


            if (oItem.Type == localConfig.Globals.Type_Object)
            {
                var searchRightLeftInformal = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItem.GUID
                    }


                };
                result = dbReader_ObjectRelationsRightLeftInformal.GetDataObjectRel(searchRightLeftInformal);
            }
            

            int idNode = 0;
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                lock (serviceAgentLocker)
                {
                    if (oItem.Type == localConfig.Globals.Type_Object)
                    {
                        RightLeftInformalNodes = (from rightLeftInformalNode in dbReader_ObjectRelationsRightLeftInformal.ObjectRels
                                                  join rightLeftNode in RightLeftNodes on new { IdRelationType = rightLeftInformalNode.ID_RelationType, IdLeftClass = rightLeftInformalNode.ID_Parent_Object } equals
                                                     new { IdRelationType = rightLeftNode.IdRelationType, IdLeftClass = rightLeftNode.IdLeft } into rightLeftNodes
                                                  from rightLeftNode in rightLeftNodes.DefaultIfEmpty()
                                                  where rightLeftNode == null
                                                  select rightLeftInformalNode).GroupBy(rightLeftInformal => new { IdClass_Left = rightLeftInformal.ID_Parent_Object, NameClass_left = rightLeftInformal.Name_Parent_Object, IdRelationType = rightLeftInformal.ID_RelationType, NameRelationType = rightLeftInformal.Name_RelationType }).Select(groupItem => new ObjectRelNode(false, true)
                                                  {
                                                      IdNode = "relbackwinf" + (idNode++).ToString(),
                                                      IdLeft = groupItem.Key.IdClass_Left,
                                                      NameLeft = groupItem.Key.NameClass_left,
                                                      IdRelationType = groupItem.Key.IdRelationType,
                                                      NameRelationType = groupItem.Key.NameRelationType,
                                                      IdRight = oItem.GUID,
                                                      NameRight = oItem.Name,
                                                      Count = groupItem.Count(),
                                                      NodeType = NodeType.NodeBackwardInformal
                                                  }).ToList();
                    }
                    else
                    {
                        rightLeftInformalNodes = new List<ObjectRelNode>();
                    }
                    
                }
                

            }

            return result;
        }

        public clsOntologyItem GetOItem(string idItem, string typeItem)
        {
            return dbReader_OItem.GetOItem(idItem, typeItem);
        }

        private void Initialize()
        {
            dbReader_ClassAttributes = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ObjectAttributes = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ClassRelationsLeftRight = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ObjectRelationsLeftRight = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ClassRelationsRightLeft = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ObjectRelationsRightLeft = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ClassRelationsLeftRightInformal = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ObjectRelationsLeftRightInformal = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ClassRelationsRightLeftInformal = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ObjectRelationsRightLeftInformal = new OntologyModDBConnector(localConfig.Globals);
            dbReader_OItem = new OntologyModDBConnector(localConfig.Globals);
        }

    }
}
