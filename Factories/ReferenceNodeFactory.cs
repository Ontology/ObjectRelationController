﻿using Newtonsoft.Json;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelationController.Factories
{
    
    public class ReferenceNodeFactory : NotifyPropertyChange
    {
        private clsLogStates logStates = new clsLogStates();
        public clsOntologyItem CreateClassAttributeJson(List<ClassAttributeNode> classAttributeNodes, SessionFile sessionFile)
        {
            using (var jsonWriter = new JsonTextWriter(sessionFile.StreamWriter))
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("items");
                jsonWriter.WriteStartArray();

                classAttributeNodes.ForEach(clsAttNode =>
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Id");
                    jsonWriter.WriteValue(clsAttNode.IdNode);
                    jsonWriter.WritePropertyName("Label");
                    jsonWriter.WriteValue(System.Web.HttpUtility.HtmlEncode(clsAttNode.NodeName));
                    jsonWriter.WritePropertyName("Color");
                    jsonWriter.WriteValue(clsAttNode.Color);
                    jsonWriter.WritePropertyName("Icon");
                    jsonWriter.WriteValue(clsAttNode.Class);
                    jsonWriter.WritePropertyName("NodeType");
                    jsonWriter.WriteValue((int)NodeType.AttributeRel);
                    jsonWriter.WriteEndObject();
                });

                jsonWriter.WriteEndArray();
                jsonWriter.WriteEndObject();
            }

            return logStates.LogState_Success.Clone();
        }

        public clsOntologyItem CreateClassRelation(List<ObjectRelNode> nodes, SessionFile sessionFile, NodeType nodeType)
        {
            if (nodes == null) return logStates.LogState_Nothing.Clone();
            using (var jsonWriter = new JsonTextWriter(sessionFile.StreamWriter))
            {
                jsonWriter.WriteStartObject();
                jsonWriter.WritePropertyName("items");
                jsonWriter.WriteStartArray();

                nodes.ForEach(leftRightNode =>
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Id");
                    jsonWriter.WriteValue(leftRightNode.IdNode);
                    jsonWriter.WritePropertyName("Label");
                    jsonWriter.WriteValue(System.Web.HttpUtility.HtmlEncode(leftRightNode.NodeName));
                    jsonWriter.WritePropertyName("Color");
                    jsonWriter.WriteValue(leftRightNode.Color);
                    jsonWriter.WritePropertyName("Icon");
                    jsonWriter.WriteValue(leftRightNode.Class);
                    jsonWriter.WritePropertyName("NodeType");
                    jsonWriter.WriteValue((int)nodeType);
                    jsonWriter.WriteEndObject();
                });

                jsonWriter.WriteEndArray();
                jsonWriter.WriteEndObject();
            }

            return logStates.LogState_Success.Clone();
        }

    }
}
