﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ObjectRelationController.Notifications {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class NotifyChanges {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal NotifyChanges() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ObjectRelationController.Notifications.NotifyChanges", typeof(NotifyChanges).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResultDataAttributes.
        /// </summary>
        internal static string Elastic_ResultDataAttributes {
            get {
                return ResourceManager.GetString("Elastic_ResultDataAttributes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResultDataLeftRight.
        /// </summary>
        internal static string Elastic_ResultDataLeftRight {
            get {
                return ResourceManager.GetString("Elastic_ResultDataLeftRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResultDataLeftRightInformal.
        /// </summary>
        internal static string Elastic_ResultDataLeftRightInformal {
            get {
                return ResourceManager.GetString("Elastic_ResultDataLeftRightInformal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResultDataRightLeft.
        /// </summary>
        internal static string Elastic_ResultDataRightLeft {
            get {
                return ResourceManager.GetString("Elastic_ResultDataRightLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResultDataRightLeftInformal.
        /// </summary>
        internal static string Elastic_ResultDataRightLeftInformal {
            get {
                return ResourceManager.GetString("Elastic_ResultDataRightLeftInformal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_View.
        /// </summary>
        internal static string View_Text_View {
            get {
                return ResourceManager.GetString("View_Text_View", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_ClassAttributeNodes.
        /// </summary>
        internal static string View_Url_ClassAttributeNodes {
            get {
                return ResourceManager.GetString("View_Url_ClassAttributeNodes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ContextMenuEntry_ContextMenuEntryChange.
        /// </summary>
        internal static string ViewModel_ContextMenuEntry_ContextMenuEntryChange {
            get {
                return ResourceManager.GetString("ViewModel_ContextMenuEntry_ContextMenuEntryChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Int_OrderId.
        /// </summary>
        internal static string ViewModel_Int_OrderId {
            get {
                return ResourceManager.GetString("ViewModel_Int_OrderId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsChecked_NextOrderId.
        /// </summary>
        internal static string ViewModel_IsChecked_NextOrderId {
            get {
                return ResourceManager.GetString("ViewModel_IsChecked_NextOrderId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsEnabled_OrderId.
        /// </summary>
        internal static string ViewModel_IsEnabled_OrderId {
            get {
                return ResourceManager.GetString("ViewModel_IsEnabled_OrderId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsSuccessful_Login.
        /// </summary>
        internal static string ViewModel_IsSuccessful_Login {
            get {
                return ResourceManager.GetString("ViewModel_IsSuccessful_Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IsToggled_Listen.
        /// </summary>
        internal static string ViewModel_IsToggled_Listen {
            get {
                return ResourceManager.GetString("ViewModel_IsToggled_Listen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to JqxDataSource_ContextMenu.
        /// </summary>
        internal static string ViewModel_JqxDataSource_ContextMenu {
            get {
                return ResourceManager.GetString("ViewModel_JqxDataSource_ContextMenu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ListenToggleItem_ListenToggleNode.
        /// </summary>
        internal static string ViewModel_ListenToggleItem_ListenToggleNode {
            get {
                return ResourceManager.GetString("ViewModel_ListenToggleItem_ListenToggleNode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NavigationNodeChangeItem_Change.
        /// </summary>
        internal static string ViewModel_NavigationNodeChangeItem_Change {
            get {
                return ResourceManager.GetString("ViewModel_NavigationNodeChangeItem_Change", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RelationNode_Selected.
        /// </summary>
        internal static string ViewModel_RelationNode_Selected {
            get {
                return ResourceManager.GetString("ViewModel_RelationNode_Selected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_NodeAttributes.
        /// </summary>
        internal static string ViewModel_Text_NodeAttributes {
            get {
                return ResourceManager.GetString("ViewModel_Text_NodeAttributes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_NodeLeftRight.
        /// </summary>
        internal static string ViewModel_Text_NodeLeftRight {
            get {
                return ResourceManager.GetString("ViewModel_Text_NodeLeftRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_NodeLeftRightInformal.
        /// </summary>
        internal static string ViewModel_Text_NodeLeftRightInformal {
            get {
                return ResourceManager.GetString("ViewModel_Text_NodeLeftRightInformal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_NodeOtherBackward.
        /// </summary>
        internal static string ViewModel_Text_NodeOtherBackward {
            get {
                return ResourceManager.GetString("ViewModel_Text_NodeOtherBackward", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Text_NodeRightLeft.
        /// </summary>
        internal static string ViewModel_Text_NodeRightLeft {
            get {
                return ResourceManager.GetString("ViewModel_Text_NodeRightLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ToolTip_NextOrderId.
        /// </summary>
        internal static string ViewModel_ToolTip_NextOrderId {
            get {
                return ResourceManager.GetString("ViewModel_ToolTip_NextOrderId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_AttributeAdd.
        /// </summary>
        internal static string ViewModel_Url_AttributeAdd {
            get {
                return ResourceManager.GetString("ViewModel_Url_AttributeAdd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_LeftRightInformalNodes.
        /// </summary>
        internal static string ViewModel_Url_LeftRightInformalNodes {
            get {
                return ResourceManager.GetString("ViewModel_Url_LeftRightInformalNodes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_LeftRightNodes.
        /// </summary>
        internal static string ViewModel_Url_LeftRightNodes {
            get {
                return ResourceManager.GetString("ViewModel_Url_LeftRightNodes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_Others.
        /// </summary>
        internal static string ViewModel_Url_Others {
            get {
                return ResourceManager.GetString("ViewModel_Url_Others", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Url_RightLeftNodes.
        /// </summary>
        internal static string ViewModel_Url_RightLeftNodes {
            get {
                return ResourceManager.GetString("ViewModel_Url_RightLeftNodes", resourceCulture);
            }
        }
    }
}
