﻿using Newtonsoft.Json;
using ObjectRelationController.Factories;
using ObjectRelationController.Services;
using ObjectRelationController.StateMachine;
using ObjectRelationController.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using ReferenceTreeController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelationController.Controllers
{
    public class ObjectRelationController : ObjectRelationViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private ReferenceNodeFactory referenceNodeFactory;

        private TranslationController translationController = new TranslationController();

        private ServiceAgent_Elastic serviceAgent_Elastic;

        public IControllerStateMachine StateMachine { get; private set; }
        public ObjectRelationStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ObjectRelationStateMachine)StateMachine;
            }
        }

        private MenuEntry listenMenuEntry;
        private MenuEntry addAttribute;

        private ReferenceTreeController.clsLocalConfig localConfig;

        private clsTransaction transactionManager;
        private clsRelationConfig relationConfigManager;

        private string dedicatedSender;

        public ObjectRelationController()
        {
            // local configuration
            localConfig = (ReferenceTreeController.clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new ReferenceTreeController.clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            PropertyChanged += ObjectRelationController_PropertyChanged;
            


        }
        private void Initialize()
        {
            serviceAgent_Elastic = new ServiceAgent_Elastic(localConfig);
            referenceNodeFactory = new ReferenceNodeFactory();
            transactionManager = new clsTransaction(localConfig.Globals);
            relationConfigManager = new clsRelationConfig(localConfig.Globals);
            serviceAgent_Elastic.PropertyChanged += ServiceAgent_Elastic_PropertyChanged;

            var stateMachine = new ObjectRelationStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected, serviceAgent_Elastic);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.sendClassAttributeNode += StateMachine_sendClassAttributeNode;
            stateMachine.sendObjRelLeftRight += StateMachine_sendObjRelLeftRight;
            stateMachine.sendObjRelRightLeft += StateMachine_sendObjRelRightLeft;
            stateMachine.sendObjRelLeftRightInformal += StateMachine_sendObjRelLeftRightInformal;
            stateMachine.sendObjRelRightLeftInformal += StateMachine_sendObjRelRightLeftInformal;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            Text_NodeAttributes = translationController.Text_Attributes;
            Text_NodeLeftRight = translationController.Text_LeftRight;
            Text_NodeRightLeft = translationController.Text_RightLeft;
            Text_NodeLeftRightInformal = translationController.Text_LeftRightInformal;
            Text_NodeOtherBackward = translationController.Text_Other;

            listenMenuEntry = new MenuEntry
            {
                Id = Guid.NewGuid().ToString(),
                Name = translationController.MenuEntry_Listen,
                IsVisible = true,
                IsEnabled = true
            };
            addAttribute = new MenuEntry
            {
                Id = Guid.NewGuid().ToString(),
                Name = translationController.MenuEntry_AddAttribute,
                IsVisible = true,
                IsEnabled = false
            };



        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            serviceAgent_Elastic = null;
            referenceNodeFactory = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsChecked_NextOrderId = false;
            IsEnabled_OrderId = false;
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });



            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AddedAttributes,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            IsToggled_Listen = true;
            var fileNameContextMenuJson = Guid.NewGuid().ToString() + ".json";
            var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameContextMenuJson);

            var jqxMenuEntries = new List<MenuEntry>
                {
                    listenMenuEntry,
                    addAttribute
                };

            using (sessionFile.StreamWriter)
            {

                using (var jsonWriter = new JsonTextWriter(sessionFile.StreamWriter))
                {
                    var jsonString = JsonConvert.SerializeObject(jqxMenuEntries);

                    jsonWriter.WriteRaw(jsonString);
                }

                var jqxDataSource = new JqxDataSource(JsonDataType.Json)
                {
                    datafields = new List<DataField>
                    {
                        new DataField(DataFieldType.String)
                        {
                            name = "Id"
                        },
                        new DataField(DataFieldType.String)
                        {
                            name = "Name"
                        },
                        new DataField(DataFieldType.String)
                        {
                            name = "ParentId"
                        }
                    },
                    url = sessionFile.FileUri.AbsoluteUri,
                    id = "Id"
                };

                JqxDataSource_ContextMenu = jqxDataSource;
            }

            webSocketServiceAgent.SendModel();
        }

        private void StateMachine_sendObjRelRightLeftInformal(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode)
        {
            var selectionMessage = new InterServiceMessage
            {
                ChannelId = Channels.SelectedRelationNode,
                SenderId = webSocketServiceAgent.EndpointId,
                OItems = new List<clsOntologyItem>
                            {
                                oItemSelected
                            },
                GenericParameterItems = new List<object>
                            {
                                objectRelNode
                            },
                ParameterIdentification = typeof(ObjectRelNode).ToString()
            };
            webSocketServiceAgent.SendInterModMessage(selectionMessage);
            ContextMenuEntry_ContextMenuEntryChange = addAttribute;

            var leftItemId = objectRelNode.IdLeft;

            if (string.IsNullOrEmpty(leftItemId)) return;

            var oItem = serviceAgent_Elastic.GetOItem(leftItemId, localConfig.Globals.Type_Class);

            if (oItem != null)
            {
                selectionMessage = new InterServiceMessage
                {
                    ChannelId = Channels.SelectClassNode,
                    SenderId = webSocketServiceAgent.EndpointId,
                    OItems = new List<clsOntologyItem>
                                    {
                                        oItem
                                    }
                };
                webSocketServiceAgent.SendInterModMessage(selectionMessage);
            }
        }

        private void StateMachine_sendObjRelLeftRightInformal(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode)
        {
            var selectionMessage = new InterServiceMessage
            {
                ChannelId = Channels.SelectedRelationNode,
                SenderId = webSocketServiceAgent.EndpointId,
                OItems = new List<clsOntologyItem>
                            {
                                oItemSelected
                            },
                GenericParameterItems = new List<object>
                            {
                                objectRelNode
                            },
                ParameterIdentification = typeof(ObjectRelNode).ToString()
            };
            webSocketServiceAgent.SendInterModMessage(selectionMessage);
            ContextMenuEntry_ContextMenuEntryChange = addAttribute;

            var rightItemId = objectRelNode.IdRight;

            if (string.IsNullOrEmpty(rightItemId)) return;

            var oItem = serviceAgent_Elastic.GetOItem(rightItemId, localConfig.Globals.Type_Class);

            if (oItem != null)
            {
                selectionMessage = new InterServiceMessage
                {
                    ChannelId = Channels.SelectClassNode,
                    SenderId = webSocketServiceAgent.EndpointId,
                    OItems = new List<clsOntologyItem>
                                    {
                                        oItem
                                    }
                };
                webSocketServiceAgent.SendInterModMessage(selectionMessage);
            }
        }

        private void StateMachine_sendObjRelRightLeft(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode)
        {
            var selectionMessage = new InterServiceMessage
            {
                ChannelId = Channels.SelectedRelationNode,
                SenderId = webSocketServiceAgent.EndpointId,
                OItems = new List<clsOntologyItem>
                            {
                                oItemSelected
                            },
                GenericParameterItems = new List<object>
                            {
                                objectRelNode
                            },
                ParameterIdentification = typeof(ObjectRelNode).ToString()
            };
            webSocketServiceAgent.SendInterModMessage(selectionMessage);
            ContextMenuEntry_ContextMenuEntryChange = addAttribute;

            var leftItemId = objectRelNode.IdLeft;

            if (string.IsNullOrEmpty(leftItemId)) return;

            var oItem = serviceAgent_Elastic.GetOItem(leftItemId, localConfig.Globals.Type_Class);

            if (oItem != null)
            {
                selectionMessage = new InterServiceMessage
                {
                    ChannelId = Channels.SelectClassNode,
                    SenderId = webSocketServiceAgent.EndpointId,
                    OItems = new List<clsOntologyItem>
                                    {
                                        oItem
                                    }
                };
                webSocketServiceAgent.SendInterModMessage(selectionMessage);
            }
        }

        private void StateMachine_sendObjRelLeftRight(clsOntologyItem oItemSelected, ObjectRelNode objectRelNode)
        {
            var selectionMessage = new InterServiceMessage
            {
                ChannelId = Channels.SelectedRelationNode,
                SenderId = webSocketServiceAgent.EndpointId,
                OItems = new List<clsOntologyItem>
                            {
                                oItemSelected
                            },
                GenericParameterItems = new List<object>
                            {
                                objectRelNode
                            },
                ParameterIdentification = typeof(ObjectRelNode).ToString()
            };
            webSocketServiceAgent.SendInterModMessage(selectionMessage);
            ContextMenuEntry_ContextMenuEntryChange = addAttribute;

            var rightItemId = objectRelNode.IdRight;

            if (string.IsNullOrEmpty(rightItemId)) return;

            var oItem = serviceAgent_Elastic.GetOItem(rightItemId, localConfig.Globals.Type_Class);

            if (oItem != null)
            {
                selectionMessage = new InterServiceMessage
                {
                    ChannelId = Channels.SelectClassNode,
                    SenderId = webSocketServiceAgent.EndpointId,
                    OItems = new List<clsOntologyItem>
                                    {
                                        oItem
                                    }
                };
                webSocketServiceAgent.SendInterModMessage(selectionMessage);
            }
        }

        private void StateMachine_sendClassAttributeNode(clsOntologyItem oItemSelected, ClassAttributeNode classAttributeNode)
        {
            addAttribute.IsEnabled = true;
            var selectionMessage = new InterServiceMessage
            {
                ChannelId = Channels.SelectedRelationNode,
                SenderId = webSocketServiceAgent.EndpointId,
                OItems = new List<clsOntologyItem>
                            {
                                oItemSelected
                            },
                GenericParameterItems = new List<object>
                            {
                                classAttributeNode
                            },
                ParameterIdentification = typeof(ClassAttributeNode).ToString()
            };

            webSocketServiceAgent.SendInterModMessage(selectionMessage);
            ContextMenuEntry_ContextMenuEntryChange = addAttribute;
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            Text_View = oItemSelected.Name;
            if (oItemSelected.Type == localConfig.Globals.Type_Object)
            {
                var classItem = serviceAgent_Elastic.GetOItem(oItemSelected.GUID_Parent, localConfig.Globals.Type_Class);

                Text_View += " (" + classItem.Name + ")";
            }


            serviceAgent_Elastic.LoadAttributes(oItemSelected);
            serviceAgent_Elastic.LoadLeftRight(oItemSelected);
            serviceAgent_Elastic.LoadRightLeft(oItemSelected);
            serviceAgent_Elastic.LoadLeftRightInformal(oItemSelected);
        }

        private void StateMachine_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ServiceAgent_Elastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultDataAttributes)
            {
                var fileName = Guid.NewGuid().ToString() + ".json";
                var sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);
                var result = referenceNodeFactory.CreateClassAttributeJson(serviceAgent_Elastic.ClassAttributeNodes, sessionFile);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var uri = sessionFile.FileUri;
                    Url_ClassAttributeNodes = uri.AbsoluteUri;
                }
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultDataLeftRight)
            {
                var fileName = Guid.NewGuid().ToString() + ".json";
                var sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);
                var result = referenceNodeFactory.CreateClassRelation(serviceAgent_Elastic.LeftRightNodes, sessionFile, NodeType.NodeForwardFormal);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var uri = sessionFile.FileUri;
                    Url_LeftRightNodes = uri.AbsoluteUri;
                }
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultDataRightLeft)
            {
                var fileName = Guid.NewGuid().ToString() + ".json";
                var sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);
                var result = referenceNodeFactory.CreateClassRelation(serviceAgent_Elastic.RightLeftNodes, sessionFile, NodeType.NodeBackwardFormal);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var uri = sessionFile.FileUri;
                    Url_RightLeftNodes = uri.AbsoluteUri;
                }

                fileName = Guid.NewGuid().ToString() + ".json";
                sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);
                result = referenceNodeFactory.CreateClassRelation(serviceAgent_Elastic.RightLeftInformalNodes, sessionFile, NodeType.NodeBackwardInformal);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var uri = sessionFile.FileUri;
                    Url_Others = uri.AbsoluteUri;
                }
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultDataLeftRightInformal)
            {
                var fileName = Guid.NewGuid().ToString() + ".json";
                var sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);
                var result = referenceNodeFactory.CreateClassRelation(serviceAgent_Elastic.LeftRightInformalNodes, sessionFile, NodeType.NodeForwardInformal);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var uri = sessionFile.FileUri;
                    Url_LeftRightInformalNodes = uri.AbsoluteUri;
                }
            }
        }

        private void ObjectRelationController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));


            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsChecked_NextOrderId)
            {
                IsEnabled_OrderId = IsChecked_NextOrderId;
            }

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

           
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            Initialize();
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "RelationNode_Selected")
                {
                    
                    addAttribute.IsEnabled = false;

                    var selectedNodeObject = webSocketServiceAgent.Request["SelectedNode"];
                    if (selectedNodeObject != null)
                    {
                        var selectedNodeString = selectedNodeObject.ToString();
                        LocalStateMachine.SetRelationNode(selectedNodeString);
                        
                    }


                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "ClickedMenuItem")
                {
                    if (webSocketServiceAgent.Request["Id"].ToString() == listenMenuEntry.Id)
                    {

                        string toggleItemId = "";
                        if (ListenToggleItem_ListenToggleNode != null)
                        {
                            toggleItemId = ListenToggleItem_ListenToggleNode.Id;
                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = ListenToggleItem_ListenToggleNode.Id,
                                Text = ListenToggleItem_ListenToggleNode.Text,
                                ListenMode = false
                            };
                            ListenToggleItem_ListenToggleNode = null;
                        }
                        if (LocalStateMachine.SelectedClassAttribute != null && LocalStateMachine.SelectedClassAttribute.IdNode != toggleItemId)
                        {
                            //ListenToggleItem_ListenToggleNode = new ListenToggleItem
                            //{
                            //    Id = selectedClassAttribute.IdNode,
                            //    Text = selectedClassAttribute.NodeName,
                            //    ListenMode = true
                            //};
                        }
                        else if (LocalStateMachine.SelectedObjectRelNodeLeftright != null && LocalStateMachine.SelectedObjectRelNodeLeftright.IdNode != toggleItemId)
                        {
                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = LocalStateMachine.SelectedObjectRelNodeLeftright.IdNode,
                                Text = LocalStateMachine.SelectedObjectRelNodeLeftright.NodeName,
                                ListenMode = true,
                                Color = LocalStateMachine.SelectedObjectRelNodeLeftright.Color,
                                Class = LocalStateMachine.SelectedObjectRelNodeLeftright.Class
                            };
                        }
                        else if (LocalStateMachine.SelectedObjectRelNodeRightLeft != null && LocalStateMachine.SelectedObjectRelNodeRightLeft.IdNode != toggleItemId)
                        {
                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = LocalStateMachine.SelectedObjectRelNodeRightLeft.IdNode,
                                Text = LocalStateMachine.SelectedObjectRelNodeRightLeft.NodeName,
                                ListenMode = true,
                                Color = LocalStateMachine.SelectedObjectRelNodeRightLeft.Color,
                                Class = LocalStateMachine.SelectedObjectRelNodeRightLeft.Class
                            };

                        }
                        else if (LocalStateMachine.SelectedObjectRelNodeLeftRightInformal != null && LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.IdNode != toggleItemId)
                        {
                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.IdNode,
                                Text = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.NodeName,
                                ListenMode = true,
                                Color = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.Color,
                                Class = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.Class
                            };
                        }
                        else if (LocalStateMachine.SelectedObjectRelNodeRightLeftInformal != null && LocalStateMachine.SelectedObjectRelNodeRightLeftInformal.IdNode != toggleItemId)
                        {
                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = LocalStateMachine.SelectedObjectRelNodeRightLeftInformal.IdNode,
                                Text = LocalStateMachine.SelectedObjectRelNodeRightLeftInformal.NodeName,
                                ListenMode = true,
                                Color = LocalStateMachine.SelectedObjectRelNodeRightLeftInformal.Color,
                                Class = LocalStateMachine.SelectedObjectRelNodeRightLeftInformal.Class
                            };
                        }
                    }
                    else if (webSocketServiceAgent.Request["Id"].ToString() == addAttribute.Id)
                    {
                        if (LocalStateMachine.SelectedClassAttribute == null || LocalStateMachine.OItemSelected == null)
                        {
                            addAttribute.IsEnabled = false;
                            ContextMenuEntry_ContextMenuEntryChange = addAttribute;
                            return;
                        }

                        var attributNewView = ModuleDataExchanger.GetViewById("459d18a3fd794cdeadc6c05ab3ce1bec");
                        if (attributNewView == null) return;

                        Url_AttributeAdd = "../" + attributNewView.NameCommandLineRun + "?Object=" + LocalStateMachine.OItemSelected.GUID + "&Class=" + LocalStateMachine.SelectedClassAttribute.IdClass + "&AttributeType=" + LocalStateMachine.SelectedClassAttribute.IdAttributeType;

                        //var interServiceMessage = new InterServiceMessage
                        //{
                        //    ChannelId = Channels.AddAttribute,
                        //    GenericParameterItems = new List<object> { selectedClassAttribute },
                        //    OItems = new List<clsOntologyItem> { oItemSelected }
                        //};

                        //webSocketServiceAgent.SendInterModMessage(interServiceMessage);

                    }
                }



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    
                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_DedicatedSenderArgument)
            {
                if (webSocketServiceAgent.DedicatedSenderArgument != null)
                {
                    var interServiceMessage = new InterServiceMessage
                    {
                        ChannelId = Channels.ReceiverOfDedicatedSender,
                        ReceiverId = webSocketServiceAgent.DedicatedSenderArgument.Value
                    };

                    webSocketServiceAgent.SendInterModMessage(interServiceMessage);
                }
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var idObject = webSocketServiceAgent.ObjectArgument.Value;

                if (!localConfig.Globals.is_GUID(idObject)) return;

                var oItem = serviceAgent_Elastic.GetOItem(idObject, localConfig.Globals.Type_Object);

                if (oItem == null) return;

                LocalStateMachine.SetItemSelected(oItem);

                
            }

        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void SavedAttribute(clsObjectAtt objAtt)
        {
            if (objAtt != null && LocalStateMachine.SelectedClassAttribute != null && objAtt.ID_AttributeType == LocalStateMachine.SelectedClassAttribute.IdAttributeType &&
                objAtt.ID_Class == LocalStateMachine.SelectedClassAttribute.IdClass)
            {
                LocalStateMachine.SelectedClassAttribute.Count++;
                NavigationNodeChangeItem_Change = new NavigationNodeChangeItem
                {
                    Id = LocalStateMachine.SelectedClassAttribute.IdNode,
                    Text = LocalStateMachine.SelectedClassAttribute.NodeName,
                    ListenMode = false,
                    Color = LocalStateMachine.SelectedClassAttribute.Color,
                    Class = LocalStateMachine.SelectedClassAttribute.Class
                };

                webSocketServiceAgent.SendCommand("CloseNewAttributeWindow");
            }
        }

       

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.SelectedClassNode)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;
                
                var oItemSelected = oItem;

                oItemSelected = serviceAgent_Elastic.GetOItem(oItemSelected.GUID, localConfig.Globals.Type_Class);
                LocalStateMachine.SetItemSelected(oItemSelected);
            }
            if (message.ChannelId == Channels.ParameterList)
            {
                var oItem = message.OItems.LastOrDefault();

                if (oItem == null) return;
                if (oItem == null) return;

                var oItemSelected = oItem;

                LocalStateMachine.SetItemSelected(oItemSelected);

            }
            else if (message.ChannelId == Channels.AppliedObjects)
            {
                var objects = message.OItems;

                if (objects != null && objects.Any())
                {
                    var orderId = 1;
                    if (IsChecked_NextOrderId)
                    {
                        orderId = Int_OrderId;
                    }
                    if (LocalStateMachine.SelectedObjectRelNodeLeftright != null)
                    {
                        var relationType = new clsOntologyItem
                        {
                            GUID = LocalStateMachine.SelectedObjectRelNodeLeftright.IdRelationType,
                            Name = LocalStateMachine.SelectedObjectRelNodeLeftright.NameRelationType,
                            Type = localConfig.Globals.Type_RelationType
                        };

                        var relationItems = objects.Select(obj => 
                        {
                            var rel = relationConfigManager.Rel_ObjectRelation(LocalStateMachine.OItemSelected, obj, relationType, orderId: orderId);
                            if (IsChecked_NextOrderId)
                            {
                                orderId++;
                            }
                            return rel;
                        }).ToList();

                        transactionManager.ClearItems();
                        var result = localConfig.Globals.LState_Success.Clone();

                        foreach(var relItm in relationItems)
                        {
                            result = transactionManager.do_Transaction(relItm);
                            if (result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                break;
                            }
                        }

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            LocalStateMachine.SelectedObjectRelNodeLeftright.Count = relationItems.Count;

                            
                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = LocalStateMachine.SelectedObjectRelNodeLeftright.IdNode,
                                Text = LocalStateMachine.SelectedObjectRelNodeLeftright.NodeName,
                                ListenMode = true,
                                Color = LocalStateMachine.SelectedObjectRelNodeLeftright.Color,
                                Class = LocalStateMachine.SelectedObjectRelNodeLeftright.Class
                            };
                        }
                    }
                    else if (LocalStateMachine.SelectedObjectRelNodeRightLeft != null)
                    {
                        var relationType = new clsOntologyItem
                        {
                            GUID = LocalStateMachine.SelectedObjectRelNodeRightLeft.IdRelationType,
                            Name = LocalStateMachine.SelectedObjectRelNodeRightLeft.NameRelationType,
                            Type = localConfig.Globals.Type_RelationType
                        };
                        var relationItems = objects.Select(obj =>
                        {
                            var rel = relationConfigManager.Rel_ObjectRelation(obj, LocalStateMachine.OItemSelected, relationType, orderId: orderId);
                            if (IsChecked_NextOrderId)
                            {
                                orderId++;
                            }
                            return rel;
                        }).ToList();

                        transactionManager.ClearItems();

                        var result = localConfig.Globals.LState_Success.Clone();
                        foreach (var relItm in relationItems)
                        {
                            result = transactionManager.do_Transaction(relItm);
                            if (result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                break;
                            }
                        }

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            LocalStateMachine.SelectedObjectRelNodeRightLeft.Count = relationItems.Count;

                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = LocalStateMachine.SelectedObjectRelNodeRightLeft.IdNode,
                                Text = LocalStateMachine.SelectedObjectRelNodeRightLeft.NodeName,
                                ListenMode = true,
                                Color = LocalStateMachine.SelectedObjectRelNodeRightLeft.Color,
                                Class = LocalStateMachine.SelectedObjectRelNodeRightLeft.Class
                            };
                        }

                    }
                    else if (LocalStateMachine.SelectedObjectRelNodeLeftRightInformal != null)
                    {
                        var relationType = new clsOntologyItem
                        {
                            GUID = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.IdRelationType,
                            Name = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.NameRelationType,
                            Type = localConfig.Globals.Type_RelationType
                        };

                        var relationItems = objects.Select(obj =>
                        {
                            var rel = relationConfigManager.Rel_ObjectRelation(LocalStateMachine.OItemSelected, obj, relationType, orderId: orderId);
                            if (IsChecked_NextOrderId)
                            {
                                orderId++;
                            }
                            return rel;
                        }).ToList();

                        transactionManager.ClearItems();
                        var result = localConfig.Globals.LState_Success.Clone();

                        foreach (var relItm in relationItems)
                        {
                            result = transactionManager.do_Transaction(relItm);
                            if (result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                break;
                            }
                        }

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.Count = relationItems.Count;


                            ListenToggleItem_ListenToggleNode = new NavigationNodeChangeItem
                            {
                                Id = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.IdNode,
                                Text = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.NodeName,
                                ListenMode = true,
                                Color = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.Color,
                                Class = LocalStateMachine.SelectedObjectRelNodeLeftRightInformal.Class
                            };
                        }
                    }
                    else if (LocalStateMachine.SelectedObjectRelNodeRightLeftInformal != null)
                    {
                        
                    }
                }
                
            }
            else if (message.ChannelId == Channels.AddedAttributes && message.GenericParameterItems.Any())
            {
                var objAtt = Newtonsoft.Json.JsonConvert.DeserializeObject<clsObjectAtt>(message.GenericParameterItems.First().ToString());
                SavedAttribute(objAtt);
            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
