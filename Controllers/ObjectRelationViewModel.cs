﻿using ObjectRelationController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectRelationController.Controllers
{
    public class ObjectRelationViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string text_NodeAttributes;
        [ViewModel(Send = true)]
        public string Text_NodeAttributes
        {
            get { return text_NodeAttributes; }
            set
            {
                if (text_NodeAttributes == value) return;

                text_NodeAttributes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_NodeAttributes);

            }
        }

        private string text_NodeLeftRight;
        [ViewModel(Send = true)]
        public string Text_NodeLeftRight
        {
            get { return text_NodeLeftRight; }
            set
            {
                if (text_NodeLeftRight == value) return;

                text_NodeLeftRight = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_NodeLeftRight);

            }
        }

        private string text_NodeRightLeft;
        [ViewModel(Send = true)]
        public string Text_NodeRightLeft
        {
            get { return text_NodeRightLeft; }
            set
            {
                if (text_NodeRightLeft == value) return;

                text_NodeRightLeft = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_NodeRightLeft);

            }
        }

        private string text_NodeOtherBackward;
        [ViewModel(Send = true)]
        public string Text_NodeOtherBackward
        {
            get { return text_NodeOtherBackward; }
            set
            {
                if (text_NodeOtherBackward == value) return;

                text_NodeOtherBackward = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_NodeOtherBackward);

            }
        }

        private string url_ClassAttributeNodes;
        [ViewModel(Send = true)]
        public string Url_ClassAttributeNodes
        {
            get { return url_ClassAttributeNodes; }
            set
            {
                if (url_ClassAttributeNodes == value) return;

                url_ClassAttributeNodes = value;

                RaisePropertyChanged(NotifyChanges.View_Url_ClassAttributeNodes);

            }
        }

        private string url_Others;
        [ViewModel(Send = true)]
        public string Url_Others
        {
            get { return url_Others; }
            set
            {
                if (url_Others == value) return;

                url_Others = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_Others);

            }
        }

        private string url_RightLeftNodes;
        [ViewModel(Send = true)]
        public string Url_RightLeftNodes
        {
            get { return url_RightLeftNodes; }
            set
            {
                if (url_RightLeftNodes == value) return;

                url_RightLeftNodes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_RightLeftNodes);

            }
        }

        private string url_LeftRightNodes;
        [ViewModel(Send = true)]
        public string Url_LeftRightNodes
        {
            get { return url_LeftRightNodes; }
            set
            {
                if (url_LeftRightNodes == value) return;

                url_LeftRightNodes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_LeftRightNodes);

            }
        }

        private string url_LeftRightInformalNodes;
        [ViewModel(Send = true)]
        public string Url_LeftRightInformalNodes
        {
            get { return url_LeftRightInformalNodes; }
            set
            {
                if (url_LeftRightInformalNodes == value) return;

                url_LeftRightInformalNodes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_LeftRightInformalNodes);

            }
        }

        private string text_NodeLeftRightInformal;
        [ViewModel(Send = true)]
        public string Text_NodeLeftRightInformal
        {
            get { return text_NodeLeftRightInformal; }
            set
            {
                if (text_NodeLeftRightInformal == value) return;

                text_NodeLeftRightInformal = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_NodeLeftRightInformal);

            }
        }

        private ObjectRelNode relationnode_Selected;
        [ViewModel(Send = true)]
        public ObjectRelNode RelationNode_Selected
        {
            get { return relationnode_Selected; }
            set
            {
                if (relationnode_Selected == value) return;

                relationnode_Selected = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_RelationNode_Selected);

            }
        }

        private JqxDataSource jqxdatasource_ContextMenu;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenu, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Content)]
        public JqxDataSource JqxDataSource_ContextMenu
        {
            get { return jqxdatasource_ContextMenu; }
            set
            {
                if (jqxdatasource_ContextMenu == value) return;

                jqxdatasource_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_ContextMenu);

            }
        }

        private NavigationNodeChangeItem listentoggleitem_ListenToggleNode;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "listenToggleNode", ViewItemType = ViewItemType.Other)]
        public NavigationNodeChangeItem ListenToggleItem_ListenToggleNode
        {
            get { return listentoggleitem_ListenToggleNode; }
            set
            {
                if (listentoggleitem_ListenToggleNode == value) return;

                listentoggleitem_ListenToggleNode = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ListenToggleItem_ListenToggleNode);

            }
        }

        private MenuEntry contextmenuentry_ContextMenuEntryChange;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenuEntry, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Change)]
        public MenuEntry ContextMenuEntry_ContextMenuEntryChange
        {
            get { return contextmenuentry_ContextMenuEntryChange; }
            set
            {

                contextmenuentry_ContextMenuEntryChange = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ContextMenuEntry_ContextMenuEntryChange);

            }
        }

        private bool isenabled_OrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "orderId", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_OrderId
        {
            get { return isenabled_OrderId; }
            set
            {

                isenabled_OrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_OrderId);

            }
        }

        private int int_OrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NumericInput, ViewItemId = "orderId", ViewItemType = ViewItemType.Content)]
        public int Int_OrderId
        {
            get { return int_OrderId; }
            set
            {
                if (int_OrderId == value) return;

                int_OrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Int_OrderId);

            }
        }

        private string tooltip_NextOrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "nextOrderId", ViewItemType = ViewItemType.Tooltip)]
        public string ToolTip_NextOrderId
        {
            get { return tooltip_NextOrderId; }
            set
            {
                if (tooltip_NextOrderId == value) return;

                tooltip_NextOrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ToolTip_NextOrderId);

            }
        }

        private bool ischecked_NextOrderId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "nextOrderId", ViewItemType = ViewItemType.Checked)]
        public bool IsChecked_NextOrderId
        {
            get { return ischecked_NextOrderId; }
            set
            {
                if (ischecked_NextOrderId == value) return;

                ischecked_NextOrderId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_NextOrderId);

            }
        }

        private string url_AttributeAdd;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.IFrame, ViewItemId = "urlAttributeAdd", ViewItemType = ViewItemType.Content)]
		public string Url_AttributeAdd
        {
            get { return url_AttributeAdd; }
            set
            {
                if (url_AttributeAdd == value) return;

                url_AttributeAdd = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_AttributeAdd);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private NavigationNodeChangeItem navigationnodechangeitem_Change;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.NavigationBar, ViewItemId = "jqxNavigationBar", ViewItemType = ViewItemType.Change)]
		public NavigationNodeChangeItem NavigationNodeChangeItem_Change
        {
            get { return navigationnodechangeitem_Change; }
            set
            {
                if (navigationnodechangeitem_Change == value) return;

                navigationnodechangeitem_Change = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_NavigationNodeChangeItem_Change);

            }
        }

    }
}
